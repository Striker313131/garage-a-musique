export interface ISubCategory {
    id: number;
    categoryId: number;
    title: string;
    imageLink: string;
}