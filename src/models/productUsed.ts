export interface IProductUsed {
    id: number;
    brand: number;
    model: string;
    description: string;
    imageLink: string;
    price: number;
}