export interface IClient {
    id: number;
    firstname: string;
    lastname: string;
    email: string;
    address: string;
    city: string;
    province: string;
    country: string;
    phone: string;
    organization: string;
    comment: string;
    completed: boolean;
}