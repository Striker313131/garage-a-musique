export interface IContact {
    nom: string;
    email: string;
    text: string;
    tel?: string;
}