export interface ICategory {
    id: number;
    title: string;
    imageLink: string;
}