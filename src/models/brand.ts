export interface IBrand{
    id: number;
    name: string;
    logo: string;
}

export const brands: IBrand[] = [
    {
        id: 1,
        name: 'Tesla',
        logo: 'https://cdn.1min30.com/wp-content/uploads/2019/06/Tesla-symbol.jpg'
    },
    {
        id: 2,
        name: 'Audi',
        logo: 'https://i.pinimg.com/originals/6a/cf/b6/6acfb65366aea4e21d0d961d02e6fecf.png'
    },
    {
        id: 3,
        name: 'Mercedes',
        logo: 'https://i.pinimg.com/originals/2d/3b/54/2d3b5459e7bc6eefd6361685601290a4.jpg'
    },
    {
        id: 4,
        name: 'Toyota',
        logo: 'https://i.pinimg.com/originals/2d/3b/54/2d3b5459e7bc6eefd6361685601290a4.jpg'
    },
    {
        id: 5,
        name: 'Mazda',
        logo: 'https://i.pinimg.com/originals/2d/3b/54/2d3b5459e7bc6eefd6361685601290a4.jpg'
    },
    {
        id: 6,
        name: 'Mezda',
        logo: 'https://i.pinimg.com/originals/2d/3b/54/2d3b5459e7bc6eefd6361685601290a4.jpg'
    },
    {
        id: 7,
        name: 'Mozda',
        logo: 'https://i.pinimg.com/originals/2d/3b/54/2d3b5459e7bc6eefd6361685601290a4.jpg'
    },
    {
        id: 8,
        name: 'Mizda',
        logo: 'https://i.pinimg.com/originals/2d/3b/54/2d3b5459e7bc6eefd6361685601290a4.jpg'
    },
    {
        id: 9,
        name: 'Mrzda',
        logo: 'https://i.pinimg.com/originals/2d/3b/54/2d3b5459e7bc6eefd6361685601290a4.jpg'
    }
]