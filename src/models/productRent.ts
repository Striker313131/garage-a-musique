export interface IProductRent {
    id: number;
    brand: number;
    model: string;
    description: string;
    imageLink: string;
    shortTermPrice: number;
    midTermPrice: number;
    longTermPrice: number;
}