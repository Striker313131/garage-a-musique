export interface IClient {
    prenom: string;
    nom: string;
    courriel: string;
    telephone: string;
    message: string;
}