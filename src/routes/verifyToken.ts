import jwt from 'jsonwebtoken';

export default function verifyToken(req: any, res: any, next: any){
    const bearerHeader = req.headers['authorization'];
    if(typeof bearerHeader !== 'undefined'){
        const bearer = bearerHeader.split(' ');
        req.body.token = bearer[1];
        next();
    } else{
        res.status(403).json();
    }
}