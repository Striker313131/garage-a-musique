import express from 'express';
const router = express.Router();

router.use('/products', require("./products"));
router.use('/productsused', require("./productsUsed"));
router.use('/productsrent', require("./productsRent"));
router.use('/categories', require("./categories"));
router.use('/subcategories',require("./subcategories"));
router.use('/login', require("./login"));
router.use('/brands', require("./brands"));
router.use('/quotes', require("./quotes"));


router.get('/',(req,res) => {
    res.status(200).json({
        message:'Le serveur écoute'
    });
})

module.exports = router;