import express from 'express';
import * as ProductsUsed from "../database/productsUsed";
import { IProductUsed } from '../models/productUsed';
const router = express.Router();

router.get('/',(req,res) => {
    ProductsUsed.getProductsUsed()
    .then((products) => res.json(products))
    .catch((err) => res.status(400).json(err));
})

router.get('/:id',(req,res) => {
    ProductsUsed.getProductUsed(parseInt(req.params.id))
    .then((products) => res.json(products[0]))
    .catch((err) => res.status(400).json(err));
})

router.delete('/:id',(req,res) => {
    ProductsUsed.deleteProductUsed(parseInt(req.params.id))
    .then(() => res.json())
    .catch(() => res.sendStatus(400));
})

router.post('/',(req,res) => {
    ProductsUsed.addProductUsed(req.body)
    .then(() => res.json())
    .catch(() => res.sendStatus(400));
})

router.put('/:id',(req,res) => {
    const product: IProductUsed = {
        id: parseInt(req.params.id),
        brand: req.body.brand,
        description: req.body.description,
        imageLink: req.body.imageLink,
        model:req.body.model,
        price: req.body.price
    }
    ProductsUsed.updateProductUsed(product)
    .then(() => res.json())
    .catch(() => res.sendStatus(400));
})

module.exports = router;