import express from 'express';
import * as SubCategories from "../database/subcategories";
import * as Categories from "../database/categories";
import verifyToken from "./verifyToken";
const router = express.Router();

router.get('/', (req,res) => {
    SubCategories.getSubCategoriesWithCategories()
    .then((subCategories) => {
            res.json(subCategories[0]);
    })
})

router.get('/:id', (req,res) => {
    SubCategories.getSubCategory(req.params.id)
    .then((subCategories) => res.json(subCategories[0]))
})

router.delete('/:id', verifyToken, (req,res) => {
    SubCategories.deleteSubCategory(req.params.id)
    .then(() => res.json())
    .catch((err) => res.status(400).json(err));
})

router.put('/:id', verifyToken, (req,res) => {
    SubCategories.updateSubCategory({id:parseInt(req.params.id),title:req.body.title,categoryId:req.body.categoryId,imageLink:req.body.imageLink})
    .then(() => res.json())
    .catch((err) => res.status(400).json(err));
})

router.post('/', verifyToken, (req,res) => {
    SubCategories.addSubCategory({categoryId:req.body.categoryId,imageLink:req.body.imageLink, title:req.body.title})
    .then(() => res.json())
    .catch((err) => res.status(400).json());
})

module.exports = router;