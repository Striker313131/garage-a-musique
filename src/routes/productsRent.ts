import express from 'express';
import * as ProductsRent from "../database/productsRent";
import { IProductRent } from "../models/productRent";
const router = express.Router();

router.get('/',(req,res) => {
    ProductsRent.getProductsRent()
    .then((products) => res.json(products))
    .catch((err) => res.status(400).json(err));
})

router.get('/:id',(req,res) => {
    ProductsRent.getProductRent(parseInt(req.params.id))
    .then((products) => res.json(products[0]))
    .catch((err) => res.status(400).json(err));
})

router.delete('/:id',(req,res) => {
    ProductsRent.deleteProductRent(parseInt(req.params.id))
    .then(() => res.json())
    .catch(() => res.sendStatus(400));
})

router.post('/',(req,res) => {
    ProductsRent.addProductRent(req.body)
    .then(() => res.json())
    .catch(() => res.sendStatus(400));
})

router.put('/:id',(req,res) => {
    const product: IProductRent = {
        id: parseInt(req.params.id),
        brand: req.body.brand,
        description: req.body.description,
        imageLink: req.body.imageLink,
        longTermPrice: req.body.longTermPrice,
        shortTermPrice: req.body.shortTermPrice,
        midTermPrice: req.body.midTermPrice,
        model:req.body.model
    }
    ProductsRent.updateProductRent(product)
    .then(() => res.json())
    .catch(() => res.sendStatus(400));
})

module.exports = router;