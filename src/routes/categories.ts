import express from 'express';
import * as Categories from "../database/categories";
import { ICategory } from '../models/category';
import verifyToken from "./verifyToken";
const router = express.Router();

router.get('/', (req,res) => {
    Categories.getCategories()
    .then((result) => {
        res.json(result);
    })
    .catch((err) => {
        res.status(404);
    })
})

router.get('/:id', (req,res) => {
    Categories.getCategory(req.params.id)
    .then((result) => res.json(result[0]))
    .catch((err) => res.status(404).json(err))
})

router.put('/:id', verifyToken, (req,res) => {
    const category: ICategory = {
        id: parseInt(req.params.id),
        title: req.body.title,
        imageLink: req.body.imageLink
    }
    Categories.updateCategory(category)
    .then((result) => res.json(result[0]))
    .catch((err) => res.status(404).json(err))
})

router.post('/', verifyToken, (req,res) => {
    Categories.addCategory(req.body)
    .then(() => res.json())
    .catch((err) => res.status(400).json(err));
})

router.delete('/:id', verifyToken, (req,res) =>{
    Categories.deleteCategory(req.params.id)
    .then(() => res.json())
    .catch((err) => res.status(400).json(err));
})

module.exports = router;