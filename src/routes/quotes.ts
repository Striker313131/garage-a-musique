import express, { response } from 'express';
import * as Submissions from "../database/quotes";
import * as Email from "../email/email";
const router = express.Router();

router.post(`/`,(req,res) => {
    const modifiedProducts: any[] = [];
    const client = req.body.client;
    const products: any[] = req.body.products;
    Submissions.addQuote(client)
    .then((res) => {
        products.forEach(product => {
            if(product.product.id)
                Submissions.addProductToQuote(product,res)
        });
    })
    .then(() => {
        products.forEach(async (product) => {
            const newProduct = await Submissions.getQuoteData(product.product.id);
            modifiedProducts.push({
                quantity: product.quantity,
                product: newProduct[0][0]
            });
            if(modifiedProducts.length == products.length){
                Email.sendSubmission(client,modifiedProducts);
            }
        })
    })
})

router.get('/', async (req,res) => {
    Submissions.getQuotes()
    .then((submissions) => {
        res.json(submissions);
    })
})

router.get('/:id', async(req,res) => {
    if(!req.params.id) res.sendStatus(401);
    const id = req.params.id;
    const quotes = await Submissions.getQuote(id);
    const products = await Submissions.getQuoteProducts(id);
    res.json({ quote:quotes[0], products });
})

router.put('/:id', async(req,res) => {
    if(!req.params.id) res.sendStatus(401);
    await Submissions.changeQuoteStatus(req.params.id, req.body.completed);
    res.json();
})

module.exports = router;