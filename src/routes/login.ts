import express from 'express';
import { login, register } from "../database/login";
import bcrypt from 'bcrypt';
import e from 'express';
import jwt from 'jsonwebtoken';
import verifyToken from "./verifyToken";
const router = express.Router();
const saltRounds = 10;

router.post('/',(req,res) => {
    const { username, password } = req.body;
    login(username)
    .then((user) => {
        const userpass = user[0].hash;
        bcrypt.compare(password,userpass,function(err: any, result:any){
            if(result){
                delete user[0].hash;
                jwt.sign({user},'secretkey', { expiresIn: '1h'}, (err:any,token:any) => {
                    if(err) res.status(400).json(err);
                    res.json({
                        token
                    })
                })
            }
            else res.sendStatus(401);
        });
    })
    .catch(() => res.sendStatus(401));
})

router.get('/auth', verifyToken, (req,res) => {
    jwt.verify(req.body.token,'secretkey', (err:any, authData:any) => {
        if(err) {
            res.sendStatus(403);
        } else{
            res.status(200).json();
        }
    })
})

router.post('/register',(req,res) => {
    bcrypt.hash(req.body.password,saltRounds,function(err,hash){
        if(err) res.status(500).json();
        register(req.body.username,hash,req.body.email)
        .then((result) =>  res.status(200).json(true))
        .catch((err) => {
            if(err.code == "ER_DUP_ENTRY")
                res.status(401).json({ message:"Cet utilisateur existe déjà" })
            else
                res.status(400).json(false);
        })
    })
})

module.exports = router;