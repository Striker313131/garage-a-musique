import express from 'express';
import { brands, IBrand } from "../models/brand";
import * as Brands from "../database/brands";
import verifyToken from "./verifyToken";
const router = express.Router();

router.get('/', (req,res) => {
    Brands.getBrands().then((brands) => {
        console.log(brands);
        res.json(brands.sort(compare));
    });
})

router.get('/:id', (req,res) => {
    Brands.getBrand(req.params.id).then((brand) => res.json(brand[0]));
})

router.post('/', verifyToken, (req,res) => {
    Brands.addBrand(req.body)
    .then(() => res.json())
    .catch(() => res.status(400).json());
})

router.delete('/:id', verifyToken, (req,res) => {
    Brands.deleteBrand(req.params.id).then(() => res.json()).catch(() => res.status(400).json());
})

router.put('/:id', verifyToken, (req,res) => {
    Brands.updateBrand({id:parseInt(req.params.id),name:req.body.name,logo:req.body.logo}).then(() => res.json()).catch(() => res.status(400).json());
})

function compare( a: IBrand, b: IBrand ) {
    return a.name < b.name ? -1 
        : a.name > b.name ? 1 
        : 0;
  }

module.exports = router;
