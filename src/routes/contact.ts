import { app } from "../app";
import express from 'express';
import { sendEmail } from "../email/email";
import * as email from "../database/email";
const router = express.Router();


/* Page de Contact */
router.post('/contact/', (req,res) => {
    sendEmail(req.body)
    .then(() => {
        email.addEmailLog(req.body);
        res.status(200).json({
            message:'Success'
        })
    })
    .catch(() => {
        res.status(404).json({
            message:'Not found'
        })
    })
})

module.exports = router;