
import * as bodyParser from 'body-parser'
import cors from 'cors';
import * as _ from 'lodash';
import express, { response } from 'express';
import multer from 'multer';
import xlsx, { WorkSheet } from 'xlsx';
import { IProduct } from './models/product';
import schedule from 'node-schedule';
import axios from 'axios';
export const app: express.Application = express();
const path = require('path');

export let exchangeRate: IExchangeRate;

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    next();
  });

const fileFilter = function(req:any,file:any,cb:any) {
    const allowedTypes = [".xlsx",".xls"];

    if(!allowedTypes.includes(path.extname(file.originalname))){
        const error = new Error('Wrong file type');
        return cb(error,false);
    }

    cb(null, true);
}

const upload = multer({ 
    dest: './uploads',
    fileFilter,
    storage: multer.diskStorage({
        destination: function(req,file,cb){
            cb(null, 'uploads/')
        },
        filename: function(req,file,cb){
            cb(null,file.originalname);
        }
    }),
    limits: {
        fileSize: 1000000000
    }
});

app.post('/upload', upload.single('file'), (req,res) => {
    const file = "uploads/"+req.file.originalname;
    const vb = xlsx.readFile(file);
    const sheet = vb.SheetNames[0];
    res.json(xlsx.utils.sheet_to_json(vb.Sheets[sheet]));
})

interface IExchangeRate {
    CAD: number;
    USD: number;
}

schedule.scheduleJob({hour: 13} , async () => fetchExchangeRate());

const fetchExchangeRate = async () => {
    const { data : { rates: { CAD, USD } }} = await axios.get('https://api.exchangeratesapi.io/latest?base=CAD');
    exchangeRate = { CAD, USD }
}

fetchExchangeRate();

app.use(cors());
app.set('view engine', 'pug');
app.set('views', './views');
app.use(bodyParser.json({ type: "*/*"}));
app.use(bodyParser.urlencoded({ extended: true}));
app.use(require('./routes'));

app.listen(process.env.PORT);