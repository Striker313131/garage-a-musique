import * as dotenv from 'dotenv'
import { IContact } from '../models/contact'
import * as nodemailer from 'nodemailer'
import { IClient } from '../models/client';
import { IProduct } from '../models/product';
dotenv.config();

export async function sendEmail(emailInfos: IContact){
    nodemailer.createTransport({
        service: 'Gmail',
        auth: {
          user: process.env.SERVER_EMAIL,
          pass: process.env.SERVER_PASS
        }
    })
    .sendMail({
      from: '"Dev Garage à musique 👻" <eternitysgames@gmail.com>',
      to: `${process.env.SUPPORT_EMAIL}`,
      subject: "Support - Garage à musique",
      text: "", // plain text body
      html: formatText(emailInfos) // html body
    },(err,info) => {
      if(err){
        throw Error("Erreur lors de l'envoi")
      } 
    })
}

export const sendSubmission = (client: IClient, products: any[]) => {
  nodemailer.createTransport({
    service: 'Gmail',
    auth: {
      user: process.env.SERVER_EMAIL,
      pass: process.env.SERVER_PASS
    }
  })
  .sendMail({
    from: '"Dev Garage à musique 👻" <eternitysgames@gmail.com>',
    to: `${process.env.SUPPORT_EMAIL}`,
    subject: "Support - Garage à musique",
    text: "", // plain text body
    html: formatSubmission(client,products) // html body
  },(err,info) => {
    if(err){
      throw Error("Erreur lors de l'envoi")
    } 
  })
}

const formatSubmission = (client: any, products: any[]) => {
  return `
  <style>
  .header{
    background-color:gray
  }
  .row{
    border:1px solid black;
    padding: 2px 10px;
  }
  </style>
  <h3>Nouvelle Soumission</h3><br/>
  <table class="tg">
    <thead>
      <tr>
        <th class="header">Model</th>
        <th class="header">Marque</th>
        <th class="header">Sous-catégorie</th>
        <th class="header">Catégorie</th>
        <th class="header">Quantité</th>
      </tr>
    </thead>
    <tbody>
      ${
        products.map((product) => {
          return `
            <tr>
            <td class="row">${product.product.model}</td>
            <td class="row">${product.product.brandName}</td>
            <td class="row">${product.product.subCategoryName}</td>
            <td class="row">${product.product.category}</td>
            <td class="row">${product.quantity}</td>
          </tr>
          `
        })
      }
    </tbody>
  </table>
  <br/><br/>
  <span>Prénom:<b> ${client.firstname} </b></span><br/>
  <span>Nom:<b> ${client.lastname} </b></span><br/>
  <span>Email:<b> ${client.email} </b></span><br/>
  <span>Adresse:<b> ${client.address} </b></span><br/>
  <span>Ville:<b> ${client.city} </b></span><br/>
  <span>Province:<b> ${client.province} </b></span><br/>
  <span>Pays:<b> ${client.country} </b></span><br/>
  <span>Téléphone:<b> ${client.phone} </b></span><br/>
  <span>Organisation:<b> ${client.organization} </b></span><br/>
  <span>Commentaire:<b> ${client.comment} </b></span><br/>
  `

}

function formatText(emailinfos: IContact){
  return `
  <p>Nom: ${emailinfos.nom}</p>
  <p>Email: ${emailinfos.email}</p>
  ${emailinfos.tel ? `<p>Téléhpone: ${emailinfos.tel}` : ""}
  <p>Message: ${emailinfos.text}</p>
  `
}