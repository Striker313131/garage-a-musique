import { connection } from "./main";
import { IClient } from "../models/client"
const e = connection.escape;

export const addEmailLog = ({ nom, prenom, courriel, telephone, message}: IClient) => {
    const sql: string = `CALL AddContactRequest(?,?,?,?,?)`
    return connection.query(sql, [prenom,nom,courriel,telephone,message],(err: any,res: any) => {
        if(err) throw err;
        return res;
    })
}