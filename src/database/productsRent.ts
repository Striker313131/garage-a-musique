import { connection } from "./main";
import { IProductRent } from "../models/productRent";

export const getProductsRent = () => {
    return new Promise<any>((resolve, reject) => connection.query('SELECT * FROM ProductsRent', (err, res) => {
        if (err) reject(err);
        resolve(res);
    }))
}

export const getProductRent = (id: number) => {
    return new Promise<any>((resolve, reject) => connection.query('SELECT * FROM ProductsRent WHERE id=?', [id], (err, res) => {
        if (err) reject(err);
        resolve(res);
    }))
}

export const deleteProductRent = (id: number) => {
    return new Promise<any>((resolve, reject) => connection.query('DELETE FROM ProductsRent WHERE id=?', [id], (err, res) => {
        if (err) reject(err);
        resolve(res);
    }))
}

export const addProductRent = ({ brand, model, description, imageLink, longTermPrice, midTermPrice, shortTermPrice }: Omit<IProductRent, 'id'>) => {
    return new Promise<any>((resolve, reject) => {
        connection.query('INSERT INTO ProductsRent(brand,model,description,imageLink,shortTermPrice,midTermPrice,longTermPrice) values(?,?,?,?,?,?,?)',
            [brand, model, description, imageLink, shortTermPrice, midTermPrice, longTermPrice], (err, res) => {
                if (err) reject(err);
                resolve(res);
            });
    })
}

export const updateProductRent = ({ id, brand, model, description, imageLink, longTermPrice, midTermPrice, shortTermPrice }: IProductRent) => {
    return new Promise<any>((resolve, reject) => {
        connection.query('UPDATE ProductsRent SET brand=?,model=?,description=?,imageLink=?,shortTermPrice=?,midTermPrice=?,longTermPrice=? WHERE id=?',
            [brand, model, description, imageLink, shortTermPrice, midTermPrice, longTermPrice, id], (err, res) => {
                if (err) reject(err);
                resolve(res);
            })
    })
}