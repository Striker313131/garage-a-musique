import { connection } from './main';
import { ISubCategory } from "../models/subcategory";

export const getSubCategories = () => {
    return new Promise<any>((resolve,reject) => {
        connection.query('SELECT * FROM SubCategories',(err: any,res: any) => {
            if(err) reject(err);
            resolve(res);
        })
    }); 
}

export const getSubCategoriesWithCategories = () => {
    return new Promise<any>((resolve,reject) => {
        connection.query('CALL GetSubCategoriesWithCategories();',(err: any,res: any) => {
            if(err) reject(err);
            resolve(res);
        })
    }); 
}


export const getSubCategory = (id: string) => {
    return new Promise<any>((resolve,reject) => {
        connection.query('SELECT * FROM SubCategories WHERE id=?', [id],(err: any,res: any) => {
            if(err) reject(err);
            resolve(res);
        })
    }); 
}

export const addSubCategory = ({imageLink,categoryId,title}: Omit<ISubCategory,'id'>) => {
    return new Promise<any>((resolve,reject) => {
        connection.query('INSERT INTO SubCategories(categoryId,title,imageLink) VALUES(?,?,?)',[categoryId,title,imageLink], (err:any, res:any) => {
            if(err) reject(err);
            resolve(res);
        })
    })
}

export const deleteSubCategory = (id: string) => {
    return new Promise<any>((resolve,reject) => {
        connection.query('DELETE FROM SubCategories WHERE id=?', [id], (err:any,res:any) => {
            if(err) reject(err);
            resolve(res);
        })
    })
}

export const updateSubCategory = ({id,categoryId,title,imageLink}: ISubCategory) => {
    return new Promise<any>((resolve,reject) => {
        connection.query('UPDATE SubCategories SET categoryId=?,title=?,imageLink=? WHERE id=?', [categoryId,title,imageLink,id], (err:any,res:any) => {
            if(err) reject(err);
            resolve(res);
        })
    })
}