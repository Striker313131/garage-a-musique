import { connection } from "./main";
import jwt from  'jsonwebtoken';

export const login = (username: string): Promise<any> => {
    return new Promise<any>((resolve,reject) => {
        connection.query('SELECT * FROM Users WHERE username=?;', [username],(err: any,res: any) => {
            if(err) reject(err);
            resolve(res);
        })
    });
};

export const register = (username: string, password: string, email: string): Promise<any> => {
    return new Promise<string>((resolve,reject) => {
        connection.query('INSERT INTO Users(username,hash,email,admin) values(?,?,?,0);', [username,password,email],(err: any,res: any) => {
            if(err) reject(err);
            resolve(res);
        })
    });
}