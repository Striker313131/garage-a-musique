import { connection } from "./main";
import { IProduct,ProductsTest } from "../models/product"
const productsLimit: number = 25;

export const getProduct = (id: string) => {
    return new Promise<any>((resolve,reject) => {
        connection.query('SELECT Products.id, Products.brand, Brands.name as brandName, Products.model, Products.price, Products.description, Products.subCategoryId as subCategoryTitle, Products.imageLink, Products.msrp, Products.sku, SubCategories.title FROM Products LEFT JOIN Brands on Products.brand=Brands.id LEFT JOIN SubCategories on Products.subCategoryId=Subcategories.id WHERE Products.id=?',[id],(err: any,res: IProduct) => {
            if(err) reject(err);
            resolve(res);
        })
    });
}

export const getProducts = () => {
    return new Promise<any>((resolve,reject) => {
        connection.query('SELECT Products.id, Products.brand, Brands.name as brandName, Products.model, Products.price, Products.description, Products.subCategoryId as subCategoryTitle, Products.imageLink, Products.msrp, Products.sku, SubCategories.title FROM Products LEFT JOIN Brands on Products.brand=Brands.id LEFT JOIN SubCategories on Products.subCategoryId=Subcategories.id', (err:any,res:any) => {
            if(err) reject(err);
            resolve(res)
        })
    })
}

export const addProduct = async ({ model, brand, description, imageLink, price, subCategoryId, sku, msrp }: Omit<IProduct, "id">) => {
    return await new Promise<any>((resolve,reject) => {
        connection.query('INSERT INTO Products(brand,model,price,description,subCategoryId,imageLink,sku,msrp) values(?,?,?,?,?,?,?,?)', 
        [brand,model || description,price || 0,description ,subCategoryId,imageLink,sku,msrp||0.0],(err:any, res:any) => {
            if(err) reject(err);
            resolve(res);
        });
    }) 
}

export const updateProduct = async ({ id, model, brand, description, imageLink, price, subCategoryId, sku, msrp }: IProduct) => {
    return await new Promise<any>((resolve,reject) => {
        connection.query('UPDATE Products SET brand=?,model=?,price=?,description=?,subCategoryId=?,imageLink=?,sku=?,msrp=? WHERE id=?', 
        [brand,model || description,price || 0,description ,subCategoryId,imageLink,sku,msrp,id||0.0],(err:any, res:any) => {
            if(err) reject(err);
            resolve(res);
        });
    }) 
}

export const deleteProduct = (id: string) => {
    return new Promise<any>((resolve,reject) => connection.query('DELETE FROM Products WHERE id=?',[id],(err:any,res:any) => {
        if(err) reject(new Error("Erreur lors de la suppression d'un produit"));
        resolve(res);
    }))
}