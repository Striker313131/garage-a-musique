import { connection } from "./main";
import { IBrand } from "../models/brand";

export const getBrands = () => {
    return new Promise<any>((resolve,reject) => {
        connection.query('SELECT * FROM Brands',(err: any,res: any) => {
            if(err) reject(err);
            resolve(res);
        })
    })
};

export const getBrand = (id: string) => {
    return new Promise<any>((resolve,reject) => {
        connection.query('SELECT * FROM Brands WHERE Brands.id=?',[id],(err:any,res:any) => {
            if(err) reject(err);
            resolve(res);
        })
    })
}

export const getBrandByName = (brandName: number) => {
    return new Promise<any>((resolve,reject) => {
        connection.query('SELECT * FROM Brands WHERE name=?',[brandName], (err:any,res:any) => {
            if(err) reject(err);
            resolve(res);
        })
    })
} 

export const addBrand = ({ name, logo }: Omit<IBrand,"id">) => {
    return new Promise((resolve,reject) => connection.query('INSERT INTO Brands(name,logo) values(?,?)',[name,logo],(err: any,res: any) => {
        if(err) reject(err);
        resolve(res);
    }));
};

export const deleteBrand = (id: string) => {
    return new Promise((resolve,reject) => connection.query('DELETE FROM Brands WHERE id=?',[id],(err: any,res: any) => {
        if(err) reject(err);
        resolve(res);
    }));
};

export const updateBrand = ({id,logo,name}: IBrand) => {
    return new Promise((resolve,reject) => connection.query('UPDATE Brands SET logo=?, name=? WHERE id=?',[logo,name,id],(err: any,res: any) => {
        if(err) reject(err);
        resolve(res);
    }));
};