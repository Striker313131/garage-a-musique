import { connection } from "./main";
import { IClient } from "../models/soummission";
import * as _ from 'lodash';
import { values, reject } from "lodash";
import { connect } from "http2";
const date = new Date();

export const addQuote = (soummission: Omit<IClient, "id" | "completed">) => {
    const { address, comment, email,lastname,organization,country,firstname,province,phone, city} = soummission;
    const id: number = 0;
    const sql: string = "INSERT INTO Submissions(firstname,lastname,email,address,city,province,country,phone,organization,comment) values(?,?,?,?,?,?,?,?,?,?)";
    return new Promise<any>((resolve,reject) => connection.query(sql,[firstname,lastname,email,address,city,province,country,phone,organization,comment],(err: any,res: any) => {
        console.log(err);
        if(err) reject(err);
        resolve(res.insertId);
    }));
};

export const getQuotes = () => {
    return new Promise<any>((resolve,reject) =>  connection.query('SELECT * FROM Quotes',(err:any,res:any) => {
            if(err) reject(err);
            resolve(res);
        }))
}

export const getQuote = (id: string) => {
    return new Promise<any>((resolve,reject) => connection.query('SELECT * FROM Quotes where id=?',[id],(err:any,res:any) => {
        if(err) reject(err);
        resolve(res);
    }))
}

export const changeQuoteStatus = (id:string, completed:boolean) => {
    return new Promise<any>((resolve,reject) => connection.query('UPDATE Quotes SET completed=? WHERE id=?',[completed ? 1 : 0,id],(err:any,res:any) => {
        if(err) reject(err);
        resolve(res[0]);
    }))
}

export const getQuoteProducts = (id: string) => {
    const sql: string = "CALL GetProductsFromSubmission(?)";
    return new Promise<any>((resolve,reject) => connection.query(sql,[id],(err:any,res:any) => {
        if(err) reject(err);
        resolve(res[0]);
    }))
}

export const getQuoteData = (productId: number) =>  new Promise<any>((resolve,reject) =>
    connection.query('CALL GetQuoteData(?)',[productId],(err: any,res: any) => {
        if(err) reject(err);
        resolve(res);
}));

export const addProductToQuote = (product: any,id: number,) => {
    connection.query('INSERT INTO SubmissionsProducts(idProduct,idSubmission,quantity) values(?,?,?);',[product.product.id,id,product.quantity], (err: any,res: any) => {
        if(err) throw err;
        return res;
    })
};