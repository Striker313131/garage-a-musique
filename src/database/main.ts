import mysql from 'mysql';
import { DatabseIP, DatabasePort, DatabaseUsername, DatabasePassword, DatabaseName } from "../config";

export const connection = mysql.createConnection({
    host: DatabseIP,
    user: DatabaseUsername,
    password: DatabasePassword,
    port: DatabasePort
});

connection.connect((err: any) => {
    if(err) console.log(err);
    connection.query(`USE ${DatabaseName};`);
})