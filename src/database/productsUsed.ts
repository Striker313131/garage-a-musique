import { connection } from "./main";
import { IProductUsed } from "../models/productUsed";

export const getProductsUsed = () => {
    return new Promise<any>((resolve, reject) => connection.query('SELECT * FROM ProductsUsed', (err, res) => {
        if (err) reject(err);
        resolve(res);
    }))
}

export const getProductUsed = (id: number) => {
    return new Promise<any>((resolve, reject) => connection.query('SELECT * FROM ProductsUsed WHERE id=?', [id], (err, res) => {
        if (err) reject(err);
        resolve(res);
    }))
}

export const deleteProductUsed = (id: number) => {
    return new Promise<any>((resolve, reject) => connection.query('DELETE FROM ProductsUsed WHERE id=?', [id], (err, res) => {
        if (err) reject(err);
        resolve(res);
    }))
}

export const addProductUsed = ({ brand, model, description, imageLink, price }: Omit<IProductUsed, 'id'>) => {
    return new Promise<any>((resolve, reject) => {
        connection.query('INSERT INTO ProductsUsed(brand,model,description,price,imageLink) values(?,?,?,?,?)',
            [brand, model, description, price, imageLink], (err, res) => {
                if (err) reject(err);
                resolve(res);
            })
    })
}

export const updateProductUsed = ({ id, brand, model, description, imageLink, price }: IProductUsed) => {
    return new Promise<any>((resolve, reject) => {
        connection.query('UPDATE ProductsUsed SET brand=?,model=?,description=?,price=?,imageLink=? WHERE id=?', [brand, model, description, price, imageLink,id], (err, res) => {
            if (err) reject(err);
            resolve(res);
        })
    })
}