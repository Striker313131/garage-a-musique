import { connection } from './main';
import { ICategory } from "../models/category";

export const getCategories = () =>  new Promise<ICategory[]>((resolve,reject) =>
        connection.query('SELECT * FROM Categories',(err: any,res: any) => {
            if(err) reject(err);
            resolve(res);
}));

export const getCategory = (id: string) => new Promise<any>((resolve,reject) => 
        connection.query('SELECT * FROM Categories WHERE id=?',[id],(err,res) => {
            if(err) reject(err);
            resolve(res);
}));


export const getCategoryFromSubCategory = (id: string) => {
    return new Promise<any>((resolve,reject) => {
        connection.query('CALL GetCategoryFromSubCategory(?);', [id],(err: any,res: any) => {
            if(err) reject(err);
            resolve(res);
        })
    }); 
}

export const updateCategory = ({id,title,imageLink}: ICategory) => new Promise<any>((resolve,reject) => {
    const sql: string = 'UPDATE Categories SET title=?, imageLink=? WHERE id=?';
    connection.query(sql, [title,imageLink,id],(err: any,res: any) => {
       if(err) reject(err);
       resolve(res);
    });
});

export const addCategory = ({title,imageLink}: Omit<ICategory,"id">) =>  new Promise<any>((resolve,reject) => 
    connection.query('INSERT INTO Categories(title,imageLink) values(?,?)',[title,imageLink],(err: any,res: any) => {
        if(err) reject(err);
        resolve(res);
}));

export const deleteCategory = (id: string) => new Promise<any>((resolve,reject) => 
    connection.query('DELETE FROM Categories WHERE id=?',[id],(err,res) => {
        if(err) reject(err);
        resolve(res);
}));