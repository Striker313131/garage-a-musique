# Garage à musique Back-End

Le back-end du Garage à musique est une application qui sert à traiter et relayer l'information au Front-End et aux bases de données.


## Config

### Courriels
Pour configurer l'adresse courriel, allez dans .env et modifiez les variables suivantes :
```
SERVER_EMAIL = to_be_changed
SERVER_PASS = to_be_changed
SUPPORT_EMAIL = to_be_changed
SMTP_SERVER = to_be_changed
```
Remplacez to be changed par les informations requises.
