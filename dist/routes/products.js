"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const Products = __importStar(require("../database/products"));
const _ = __importStar(require("lodash"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const Brands = __importStar(require("../database/brands"));
const verifyToken_1 = __importDefault(require("./verifyToken"));
const lodash_1 = require("lodash");
const app_1 = require("../app");
const router = express_1.default.Router();
/* Page de Produits */
router.get('/', (req, res) => {
    Products.getProducts()
        .then((products) => {
        res.status(200).json({ products });
    })
        .catch((err) => {
        res.status(400).json(err);
    });
});
/* Page de Produits */
router.post('/pagination', (req, res) => {
    const productsPerPage = parseInt(req.body.productsPerPage) || 10000;
    let tempProducts = [];
    let productCount = 0;
    const { brand, searchText, currency } = req.body;
    const firstItemIndex = (req.body.page - 1) * productsPerPage;
    Products.getProducts()
        .then((products) => {
        // If there is a brand filter, we remove those who do not have the same brand.
        if (brand)
            products = _.filter(products, (product) => lodash_1.toUpper(product.brandName).includes(lodash_1.toUpper(brand)));
        // If there is a searchText, we check every property in each product for a match and filter does who do not have any match
        if (searchText && searchText.length) {
            // Take every products
            _.forEach(products, (product) => {
                let productValid = false;
                // Check if the product has any property that matches with the searchText
                for (const property in product) {
                    try {
                        // Set product as valid if there is a match
                        if (product[property].includes(searchText))
                            productValid = true;
                    }
                    catch (err) { }
                }
                if (productValid)
                    tempProducts.push(product);
            });
            products = tempProducts;
        }
        _.forEach(products, (product) => {
            product.price = convertPrice(product.price, currency || "CAD");
        });
        productCount = products.length;
        products = firstItemIndex ? products.slice(firstItemIndex, firstItemIndex + productsPerPage) : products.slice(0, productsPerPage);
        const maxPage = Math.ceil(productCount / productsPerPage);
        if (req.body.page > maxPage) {
            res.sendStatus(404);
        }
        else {
            res.status(200).json({
                products,
                totalProducts: productCount,
                maxPage
            });
        }
    })
        .catch((err) => {
        console.log(err);
        res.status(400).json(err);
    });
});
const convertPrice = (price, currency) => {
    switch (currency) {
        case "USD": return price * app_1.exchangeRate.USD;
        case "CAD": return price;
        default: return price;
    }
};
router.get('/:id/:currency', (req, res) => {
    Products.getProduct(req.params.id)
        .then((result) => {
        const product = {
            ...result[0],
            price: convertPrice(result[0].price, req.params.currency)
        };
        console.log(product);
        res.json(product);
    })
        .catch((err) => res.status(400).json(err));
});
router.post('/', verifyToken_1.default, (req, res) => {
    jsonwebtoken_1.default.verify(req.body.token, 'secretkey', async (err, authData) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            if (await Products.addProduct(req.body)) {
                res.status(200).json({
                    authData
                });
            }
            else {
                res.sendStatus(400);
            }
        }
    });
});
router.put('/:id', verifyToken_1.default, (req, res) => {
    jsonwebtoken_1.default.verify(req.body.token, 'secretkey', async (err, authData) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            const { model, description, imageLink, brand, subCategoryId, price, sku, msrp } = req.body;
            const product = {
                id: parseInt(req.params.id),
                model,
                description,
                imageLink,
                brand,
                subCategoryId,
                price,
                sku,
                msrp
            };
            if (await Products.updateProduct(product))
                res.status(200).json();
            else
                res.sendStatus(400);
        }
    });
});
router.delete('/:id', verifyToken_1.default, (req, res) => {
    if (req.params.id)
        res.status(400);
    Products.deleteProduct(req.params.id)
        .then(() => res.status(200).json())
        .catch((err) => res.status(400).json(err));
});
const addBrands = async (brands) => {
    let brandsAdded = [];
    Brands.getBrands()
        .then((res) => {
        const resultBrands = res[0];
        brands = brands.filter((brand) => !resultBrands.map((b) => b.name).includes(brand));
        brands = brands.filter((v, i) => brands.indexOf(v) === i);
        brands.forEach((brand) => {
            if (!brandsAdded.includes(brand)) {
                Brands.addBrand({ name: brand, logo: "" })
                    .then(() => brandsAdded.push(brand));
            }
        });
    });
    return brandsAdded.length;
};
router.post('/upload', async (req, res) => {
});
router.post('/upload/confirm', async (req, res) => {
    const { products, override, createBrands } = req.body;
    let brandsAdded = 0;
    let created = 0;
    let updated = 0;
    let errors = 0;
    if (products.length == 0)
        res.sendStatus(401);
    else {
        if (createBrands)
            brandsAdded = await addBrands(products.map((product) => product.brand));
        Products.getProducts()
            .then(async (dbProducts) => {
            let toUpdate = [];
            if (override)
                toUpdate = dbProducts.filter((prod) => products.map((p) => p.model).includes(prod.model));
            const toCreate = products.filter((product) => !dbProducts.map((p) => p.model).includes(product.model));
            for (let product of toCreate) {
                try {
                    const brand = await Brands.getBrandByName(product.brand);
                    const p = await Products.addProduct({ ...product, brand: brand[0][0].id });
                    if (p)
                        created++;
                    else
                        errors++;
                }
                catch (err) { }
            }
            for (let product of toUpdate) {
                const p = Products.updateProduct(product);
                if (p)
                    updated++;
                else
                    errors++;
            }
        })
            .then(() => {
            res.json({
                brandsAdded, created, updated, errors
            });
        });
    }
});
module.exports = router;
