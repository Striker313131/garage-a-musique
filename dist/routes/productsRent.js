"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const ProductsRent = __importStar(require("../database/productsRent"));
const router = express_1.default.Router();
router.get('/', (req, res) => {
    ProductsRent.getProductsRent()
        .then((products) => res.json(products))
        .catch((err) => res.status(400).json(err));
});
router.get('/:id', (req, res) => {
    ProductsRent.getProductRent(parseInt(req.params.id))
        .then((products) => res.json(products[0]))
        .catch((err) => res.status(400).json(err));
});
router.delete('/:id', (req, res) => {
    ProductsRent.deleteProductRent(parseInt(req.params.id))
        .then(() => res.json())
        .catch(() => res.sendStatus(400));
});
router.post('/', (req, res) => {
    ProductsRent.addProductRent(req.body)
        .then(() => res.json())
        .catch(() => res.sendStatus(400));
});
router.put('/:id', (req, res) => {
    const product = {
        id: parseInt(req.params.id),
        brand: req.body.brand,
        description: req.body.description,
        imageLink: req.body.imageLink,
        longTermPrice: req.body.longTermPrice,
        shortTermPrice: req.body.shortTermPrice,
        midTermPrice: req.body.midTermPrice,
        model: req.body.model
    };
    ProductsRent.updateProductRent(product)
        .then(() => res.json())
        .catch(() => res.sendStatus(400));
});
module.exports = router;
