"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const Categories = __importStar(require("../database/categories"));
const verifyToken_1 = __importDefault(require("./verifyToken"));
const router = express_1.default.Router();
router.get('/', (req, res) => {
    Categories.getCategories()
        .then((result) => {
        res.json(result);
    })
        .catch((err) => {
        res.status(404);
    });
});
router.get('/:id', (req, res) => {
    Categories.getCategory(req.params.id)
        .then((result) => res.json(result[0]))
        .catch((err) => res.status(404).json(err));
});
router.put('/:id', verifyToken_1.default, (req, res) => {
    const category = {
        id: parseInt(req.params.id),
        title: req.body.title,
        imageLink: req.body.imageLink
    };
    Categories.updateCategory(category)
        .then((result) => res.json(result[0]))
        .catch((err) => res.status(404).json(err));
});
router.post('/', verifyToken_1.default, (req, res) => {
    Categories.addCategory(req.body)
        .then(() => res.json())
        .catch((err) => res.status(400).json(err));
});
router.delete('/:id', verifyToken_1.default, (req, res) => {
    Categories.deleteCategory(req.params.id)
        .then(() => res.json())
        .catch((err) => res.status(400).json(err));
});
module.exports = router;
