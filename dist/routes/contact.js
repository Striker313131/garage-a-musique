"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const email_1 = require("../email/email");
const email = __importStar(require("../database/email"));
const router = express_1.default.Router();
/* Page de Contact */
router.post('/contact/', (req, res) => {
    email_1.sendEmail(req.body)
        .then(() => {
        email.addEmailLog(req.body);
        res.status(200).json({
            message: 'Success'
        });
    })
        .catch(() => {
        res.status(404).json({
            message: 'Not found'
        });
    });
});
module.exports = router;
