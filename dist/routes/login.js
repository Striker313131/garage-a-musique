"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const login_1 = require("../database/login");
const bcrypt_1 = __importDefault(require("bcrypt"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const verifyToken_1 = __importDefault(require("./verifyToken"));
const router = express_1.default.Router();
const saltRounds = 10;
router.post('/', (req, res) => {
    const { username, password } = req.body;
    login_1.login(username)
        .then((user) => {
        const userpass = user[0].hash;
        bcrypt_1.default.compare(password, userpass, function (err, result) {
            if (result) {
                delete user[0].hash;
                jsonwebtoken_1.default.sign({ user }, 'secretkey', { expiresIn: '1h' }, (err, token) => {
                    if (err)
                        res.status(400).json(err);
                    res.json({
                        token
                    });
                });
            }
            else
                res.sendStatus(401);
        });
    })
        .catch(() => res.sendStatus(401));
});
router.get('/auth', verifyToken_1.default, (req, res) => {
    jsonwebtoken_1.default.verify(req.body.token, 'secretkey', (err, authData) => {
        if (err) {
            res.sendStatus(403);
        }
        else {
            res.status(200).json();
        }
    });
});
router.post('/register', (req, res) => {
    bcrypt_1.default.hash(req.body.password, saltRounds, function (err, hash) {
        if (err)
            res.status(500).json();
        login_1.register(req.body.username, hash, req.body.email)
            .then((result) => res.status(200).json(true))
            .catch((err) => {
            if (err.code == "ER_DUP_ENTRY")
                res.status(401).json({ message: "Cet utilisateur existe déjà" });
            else
                res.status(400).json(false);
        });
    });
});
module.exports = router;
