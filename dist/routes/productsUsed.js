"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const ProductsUsed = __importStar(require("../database/productsUsed"));
const router = express_1.default.Router();
router.get('/', (req, res) => {
    ProductsUsed.getProductsUsed()
        .then((products) => res.json(products))
        .catch((err) => res.status(400).json(err));
});
router.get('/:id', (req, res) => {
    ProductsUsed.getProductUsed(parseInt(req.params.id))
        .then((products) => res.json(products[0]))
        .catch((err) => res.status(400).json(err));
});
router.delete('/:id', (req, res) => {
    ProductsUsed.deleteProductUsed(parseInt(req.params.id))
        .then(() => res.json())
        .catch(() => res.sendStatus(400));
});
router.post('/', (req, res) => {
    ProductsUsed.addProductUsed(req.body)
        .then(() => res.json())
        .catch(() => res.sendStatus(400));
});
router.put('/:id', (req, res) => {
    const product = {
        id: parseInt(req.params.id),
        brand: req.body.brand,
        description: req.body.description,
        imageLink: req.body.imageLink,
        model: req.body.model,
        price: req.body.price
    };
    ProductsUsed.updateProductUsed(product)
        .then(() => res.json())
        .catch(() => res.sendStatus(400));
});
module.exports = router;
