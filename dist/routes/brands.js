"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const Brands = __importStar(require("../database/brands"));
const verifyToken_1 = __importDefault(require("./verifyToken"));
const router = express_1.default.Router();
router.get('/', (req, res) => {
    Brands.getBrands().then((brands) => {
        console.log(brands);
        res.json(brands.sort(compare));
    });
});
router.get('/:id', (req, res) => {
    Brands.getBrand(req.params.id).then((brand) => res.json(brand[0]));
});
router.post('/', verifyToken_1.default, (req, res) => {
    Brands.addBrand(req.body)
        .then(() => res.json())
        .catch(() => res.status(400).json());
});
router.delete('/:id', verifyToken_1.default, (req, res) => {
    Brands.deleteBrand(req.params.id).then(() => res.json()).catch(() => res.status(400).json());
});
router.put('/:id', verifyToken_1.default, (req, res) => {
    Brands.updateBrand({ id: parseInt(req.params.id), name: req.body.name, logo: req.body.logo }).then(() => res.json()).catch(() => res.status(400).json());
});
function compare(a, b) {
    return a.name < b.name ? -1
        : a.name > b.name ? 1
            : 0;
}
module.exports = router;
