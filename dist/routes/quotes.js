"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const Submissions = __importStar(require("../database/quotes"));
const Email = __importStar(require("../email/email"));
const router = express_1.default.Router();
router.post(`/`, (req, res) => {
    const modifiedProducts = [];
    const client = req.body.client;
    const products = req.body.products;
    Submissions.addQuote(client)
        .then((res) => {
        products.forEach(product => {
            if (product.product.id)
                Submissions.addProductToQuote(product, res);
        });
    })
        .then(() => {
        products.forEach(async (product) => {
            const newProduct = await Submissions.getQuoteData(product.product.id);
            modifiedProducts.push({
                quantity: product.quantity,
                product: newProduct[0][0]
            });
            if (modifiedProducts.length == products.length) {
                Email.sendSubmission(client, modifiedProducts);
            }
        });
    });
});
router.get('/', async (req, res) => {
    Submissions.getQuotes()
        .then((submissions) => {
        res.json(submissions);
    });
});
router.get('/:id', async (req, res) => {
    if (!req.params.id)
        res.sendStatus(401);
    const id = req.params.id;
    const quotes = await Submissions.getQuote(id);
    const products = await Submissions.getQuoteProducts(id);
    res.json({ quote: quotes[0], products });
});
router.put('/:id', async (req, res) => {
    if (!req.params.id)
        res.sendStatus(401);
    await Submissions.changeQuoteStatus(req.params.id, req.body.completed);
    res.json();
});
module.exports = router;
