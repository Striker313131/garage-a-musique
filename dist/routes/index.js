"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const router = express_1.default.Router();
router.use('/products', require("./products"));
router.use('/productsused', require("./productsUsed"));
router.use('/productsrent', require("./productsRent"));
router.use('/categories', require("./categories"));
router.use('/subcategories', require("./subcategories"));
router.use('/login', require("./login"));
router.use('/brands', require("./brands"));
router.use('/quotes', require("./quotes"));
router.get('/', (req, res) => {
    res.status(200).json({
        message: 'Le serveur écoute'
    });
});
module.exports = router;
