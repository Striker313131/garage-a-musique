"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const SubCategories = __importStar(require("../database/subcategories"));
const verifyToken_1 = __importDefault(require("./verifyToken"));
const router = express_1.default.Router();
router.get('/', (req, res) => {
    SubCategories.getSubCategoriesWithCategories()
        .then((subCategories) => {
        res.json(subCategories[0]);
    });
});
router.get('/:id', (req, res) => {
    SubCategories.getSubCategory(req.params.id)
        .then((subCategories) => res.json(subCategories[0]));
});
router.delete('/:id', verifyToken_1.default, (req, res) => {
    SubCategories.deleteSubCategory(req.params.id)
        .then(() => res.json())
        .catch((err) => res.status(400).json(err));
});
router.put('/:id', verifyToken_1.default, (req, res) => {
    SubCategories.updateSubCategory({ id: parseInt(req.params.id), title: req.body.title, categoryId: req.body.categoryId, imageLink: req.body.imageLink })
        .then(() => res.json())
        .catch((err) => res.status(400).json(err));
});
router.post('/', verifyToken_1.default, (req, res) => {
    SubCategories.addSubCategory({ categoryId: req.body.categoryId, imageLink: req.body.imageLink, title: req.body.title })
        .then(() => res.json())
        .catch((err) => res.status(400).json());
});
module.exports = router;
