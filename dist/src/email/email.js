"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv = __importStar(require("dotenv"));
const nodemailer = __importStar(require("nodemailer"));
dotenv.config();
async function sendEmail(emailInfos) {
    nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: process.env.SERVER_EMAIL,
            pass: process.env.SERVER_PASS
        }
    })
        .sendMail({
        from: '"Dev Garage à musique 👻" <eternitysgames@gmail.com>',
        to: `${process.env.SUPPORT_EMAIL}`,
        subject: "Support - Garage à musique",
        text: "",
        html: formatText(emailInfos) // html body
    }, (err, info) => {
        if (err) {
            console.log(err);
            throw Error("Erreur lors de l'envoi");
        }
    });
}
exports.sendEmail = sendEmail;
function formatText(emailinfos) {
    return `
  <p>Nom: ${emailinfos.nom}</p>
  <p>Email: ${emailinfos.email}</p>
  ${emailinfos.tel ? `<p>Téléhpone: ${emailinfos.tel}` : ""}
  <p>Message: ${emailinfos.text}</p>
  `;
}
//# sourceMappingURL=email.js.map