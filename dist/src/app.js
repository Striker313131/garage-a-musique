"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const bodyParser = __importStar(require("body-parser"));
const cors_1 = __importDefault(require("cors"));
const email_1 = require("./email/email");
const mysql_1 = __importDefault(require("mysql"));
const config_ts_1 = require("../config.ts");
const connection = mysql_1.default.createConnection({
    host: config_ts_1.DatabseIP,
    user: config_ts_1.DatabaseUsername,
    password: config_ts_1.DatabasePassword
});
connection.connect((err) => {
    if (err)
        console.log("error");
    console.log("Connected");
});
const app = express_1.default();
app.use(bodyParser.json({ type: "*/*" }));
app.use(cors_1.default());
app.get('/', (req, res) => {
    res.status(200).json({
        message: 'Le serveur écoute'
    });
});
/* Page de Contact */
app.post('/contact/', (req, response) => {
    email_1.sendEmail(req.body)
        .then(() => {
        response.status(200).json({
            message: 'Success'
        });
    })
        .catch(() => {
        response.status(404).json({
            message: 'Not found'
        });
    });
});
app.listen(3000);
//# sourceMappingURL=app.js.map