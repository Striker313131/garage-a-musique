"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bodyParser = __importStar(require("body-parser"));
const cors_1 = __importDefault(require("cors"));
const express_1 = __importDefault(require("express"));
const multer_1 = __importDefault(require("multer"));
const xlsx_1 = __importDefault(require("xlsx"));
const node_schedule_1 = __importDefault(require("node-schedule"));
const axios_1 = __importDefault(require("axios"));
exports.app = express_1.default();
const path = require('path');
exports.app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    next();
});
const fileFilter = function (req, file, cb) {
    const allowedTypes = [".xlsx", ".xls"];
    if (!allowedTypes.includes(path.extname(file.originalname))) {
        const error = new Error('Wrong file type');
        return cb(error, false);
    }
    cb(null, true);
};
const upload = multer_1.default({
    dest: './uploads',
    fileFilter,
    storage: multer_1.default.diskStorage({
        destination: function (req, file, cb) {
            cb(null, 'uploads/');
        },
        filename: function (req, file, cb) {
            cb(null, file.originalname);
        }
    }),
    limits: {
        fileSize: 1000000000
    }
});
exports.app.post('/upload', upload.single('file'), (req, res) => {
    const file = "uploads/" + req.file.originalname;
    const vb = xlsx_1.default.readFile(file);
    const sheet = vb.SheetNames[0];
    res.json(xlsx_1.default.utils.sheet_to_json(vb.Sheets[sheet]));
});
node_schedule_1.default.scheduleJob({ hour: 13 }, async () => fetchExchangeRate());
const fetchExchangeRate = async () => {
    const { data: { rates: { CAD, USD } } } = await axios_1.default.get('https://api.exchangeratesapi.io/latest?base=CAD');
    exports.exchangeRate = { CAD, USD };
};
fetchExchangeRate();
exports.app.use(cors_1.default());
exports.app.set('view engine', 'pug');
exports.app.set('views', './views');
exports.app.use(bodyParser.json({ type: "*/*" }));
exports.app.use(bodyParser.urlencoded({ extended: true }));
exports.app.use(require('./routes'));
exports.app.listen(process.env.PORT);
