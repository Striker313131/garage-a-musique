"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mysql_1 = __importDefault(require("mysql"));
const config_1 = require("../config");
exports.connection = mysql_1.default.createConnection({
    host: config_1.DatabseIP,
    user: config_1.DatabaseUsername,
    password: config_1.DatabasePassword,
    port: config_1.DatabasePort
});
exports.connection.connect((err) => {
    if (err)
        console.log(err);
    exports.connection.query(`USE ${config_1.DatabaseName};`);
});
