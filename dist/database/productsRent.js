"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const main_1 = require("./main");
exports.getProductsRent = () => {
    return new Promise((resolve, reject) => main_1.connection.query('SELECT * FROM ProductsRent', (err, res) => {
        if (err)
            reject(err);
        resolve(res);
    }));
};
exports.getProductRent = (id) => {
    return new Promise((resolve, reject) => main_1.connection.query('SELECT * FROM ProductsRent WHERE id=?', [id], (err, res) => {
        if (err)
            reject(err);
        resolve(res);
    }));
};
exports.deleteProductRent = (id) => {
    return new Promise((resolve, reject) => main_1.connection.query('DELETE FROM ProductsRent WHERE id=?', [id], (err, res) => {
        if (err)
            reject(err);
        resolve(res);
    }));
};
exports.addProductRent = ({ brand, model, description, imageLink, longTermPrice, midTermPrice, shortTermPrice }) => {
    return new Promise((resolve, reject) => {
        main_1.connection.query('INSERT INTO ProductsRent(brand,model,description,imageLink,shortTermPrice,midTermPrice,longTermPrice) values(?,?,?,?,?,?,?)', [brand, model, description, imageLink, shortTermPrice, midTermPrice, longTermPrice], (err, res) => {
            if (err)
                reject(err);
            resolve(res);
        });
    });
};
exports.updateProductRent = ({ id, brand, model, description, imageLink, longTermPrice, midTermPrice, shortTermPrice }) => {
    return new Promise((resolve, reject) => {
        main_1.connection.query('UPDATE ProductsRent SET brand=?,model=?,description=?,imageLink=?,shortTermPrice=?,midTermPrice=?,longTermPrice=? WHERE id=?', [brand, model, description, imageLink, shortTermPrice, midTermPrice, longTermPrice, id], (err, res) => {
            if (err)
                reject(err);
            resolve(res);
        });
    });
};
