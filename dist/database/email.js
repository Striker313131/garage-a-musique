"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const main_1 = require("./main");
const e = main_1.connection.escape;
exports.addEmailLog = ({ nom, prenom, courriel, telephone, message }) => {
    const sql = `CALL AddContactRequest(?,?,?,?,?)`;
    return main_1.connection.query(sql, [prenom, nom, courriel, telephone, message], (err, res) => {
        if (err)
            throw err;
        return res;
    });
};
