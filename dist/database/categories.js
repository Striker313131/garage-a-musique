"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const main_1 = require("./main");
exports.getCategories = () => new Promise((resolve, reject) => main_1.connection.query('SELECT * FROM Categories', (err, res) => {
    if (err)
        reject(err);
    resolve(res);
}));
exports.getCategory = (id) => new Promise((resolve, reject) => main_1.connection.query('SELECT * FROM Categories WHERE id=?', [id], (err, res) => {
    if (err)
        reject(err);
    resolve(res);
}));
exports.getCategoryFromSubCategory = (id) => {
    return new Promise((resolve, reject) => {
        main_1.connection.query('CALL GetCategoryFromSubCategory(?);', [id], (err, res) => {
            if (err)
                reject(err);
            resolve(res);
        });
    });
};
exports.updateCategory = ({ id, title, imageLink }) => new Promise((resolve, reject) => {
    const sql = 'UPDATE Categories SET title=?, imageLink=? WHERE id=?';
    main_1.connection.query(sql, [title, imageLink, id], (err, res) => {
        if (err)
            reject(err);
        resolve(res);
    });
});
exports.addCategory = ({ title, imageLink }) => new Promise((resolve, reject) => main_1.connection.query('INSERT INTO Categories(title,imageLink) values(?,?)', [title, imageLink], (err, res) => {
    if (err)
        reject(err);
    resolve(res);
}));
exports.deleteCategory = (id) => new Promise((resolve, reject) => main_1.connection.query('DELETE FROM Categories WHERE id=?', [id], (err, res) => {
    if (err)
        reject(err);
    resolve(res);
}));
