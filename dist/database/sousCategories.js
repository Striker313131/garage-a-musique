"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const main_1 = require("./main");
exports.getSousCategories = () => {
    return main_1.connection.query('CALL GetSousCategories();', (err, res) => {
        if (err)
            throw err;
        return res;
    });
};
exports.addSousCategorie = ({ titre, categorieId, imageLink }) => {
    const sql = 'CALL AddSousCategorie(?,?,?);';
    return main_1.connection.query(sql, [categorieId, titre, imageLink], (err, res) => {
        if (err)
            throw err;
        return res;
    });
};
exports.deleteSousCategorie = (id) => {
    return main_1.connection.query('CALL DeleteSousCategorie(?);', [id], (err, res) => {
        if (err)
            throw err;
        return res;
    });
};
