"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const main_1 = require("./main");
const date = new Date();
exports.addSubmission = (soummission) => {
    const { address, comment, email, lastname, organization, country, firstname, province, phone, city } = soummission;
    const id = 0;
    const sql = "INSERT INTO Submissions(firstname,lastname,email,address,city,province,country,phone,organization,comment) values(?,?,?,?,?,?,?,?,?,?)";
    return new Promise((resolve, reject) => main_1.connection.query(sql, [firstname, lastname, email, address, city, province, country, phone, organization, comment], (err, res) => {
        console.log(err);
        if (err)
            reject(err);
        resolve(res.insertId);
    }));
};
exports.GetSubmissions = () => {
    return new Promise((resolve, reject) => main_1.connection.query('CALL GetSubmissions()', (err, res) => {
        if (err)
            reject(err);
        resolve(res[0]);
    }));
};
exports.GetSubmission = (id) => {
    const sql = "SELECT firstname, lastname, date FROM Submissions where id=?";
    return new Promise((resolve, reject) => main_1.connection.query(sql, [id], (err, res) => {
        if (err)
            reject(err);
        resolve(res[0][0]);
    }));
};
exports.GetSubmissionProducts = (id) => {
    const sql = "CALL GetProductsFromSubmission(?)";
    return new Promise((resolve, reject) => main_1.connection.query(sql, [id], (err, res) => {
        if (err)
            reject(err);
        resolve(res[0]);
    }));
};
exports.getSubmissionData = (productId) => new Promise((resolve, reject) => main_1.connection.query('CALL getSubmissionData(?)', [productId], (err, res) => {
    if (err)
        reject(err);
    resolve(res);
}));
exports.addProductToSubmission = (product, id) => {
    main_1.connection.query('CALL AddSubmissionProduct(?,?,?);', [product.product.id, id, product.quantity], (err, res) => {
        if (err)
            throw err;
        return res;
    });
};
exports.changeSoummissionStatus = (id, status) => {
    const sql = 'UPDATE Soummissions SET completed=? WHERE id=?;';
    main_1.connection.query(sql, [status, id], (err, res) => {
        if (err)
            throw err;
        return res;
    });
};
