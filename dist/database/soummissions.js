"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const main_1 = require("./main");
exports.addSoummission = (soummission) => {
    const { adresse, commentaire, courriel, nomClient, organisation, pays, prenomClient, province, telephone, ville } = soummission;
    const sql = 'CALL AddSoummission(?,?,?,?,?,?,?,?,?,?);';
    main_1.connection.query(sql, [prenomClient, nomClient, courriel, adresse, ville, province, pays, telephone, organisation, commentaire], (err, res) => {
        if (err)
            throw err;
        return res;
    });
};
exports.addProduitToSoummission = (id, productId) => {
    main_1.connection.query('CALL AddProductToSoummission(?,?);', [id, productId], (err, res) => {
        if (err)
            throw err;
        return res;
    });
};
exports.changeSoummissionStatus = (id, status) => {
    const sql = 'UPDATE Soummissions SET completed=? WHERE id=?;';
    main_1.connection.query(sql, [status, id], (err, res) => {
        if (err)
            throw err;
        return res;
    });
};
