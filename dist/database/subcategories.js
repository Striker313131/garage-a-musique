"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const main_1 = require("./main");
exports.getSubCategories = () => {
    return new Promise((resolve, reject) => {
        main_1.connection.query('SELECT * FROM SubCategories', (err, res) => {
            if (err)
                reject(err);
            resolve(res);
        });
    });
};
exports.getSubCategoriesWithCategories = () => {
    return new Promise((resolve, reject) => {
        main_1.connection.query('CALL GetSubCategoriesWithCategories();', (err, res) => {
            if (err)
                reject(err);
            resolve(res);
        });
    });
};
exports.getSubCategory = (id) => {
    return new Promise((resolve, reject) => {
        main_1.connection.query('SELECT * FROM SubCategories WHERE id=?', [id], (err, res) => {
            if (err)
                reject(err);
            resolve(res);
        });
    });
};
exports.addSubCategory = ({ imageLink, categoryId, title }) => {
    return new Promise((resolve, reject) => {
        main_1.connection.query('INSERT INTO SubCategories(categoryId,title,imageLink) VALUES(?,?,?)', [categoryId, title, imageLink], (err, res) => {
            if (err)
                reject(err);
            resolve(res);
        });
    });
};
exports.deleteSubCategory = (id) => {
    return new Promise((resolve, reject) => {
        main_1.connection.query('DELETE FROM SubCategories WHERE id=?', [id], (err, res) => {
            if (err)
                reject(err);
            resolve(res);
        });
    });
};
exports.updateSubCategory = ({ id, categoryId, title, imageLink }) => {
    return new Promise((resolve, reject) => {
        main_1.connection.query('UPDATE SubCategories SET categoryId=?,title=?,imageLink=? WHERE id=?', [categoryId, title, imageLink, id], (err, res) => {
            if (err)
                reject(err);
            resolve(res);
        });
    });
};
