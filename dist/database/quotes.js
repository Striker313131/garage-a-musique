"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const main_1 = require("./main");
const date = new Date();
exports.addQuote = (soummission) => {
    const { address, comment, email, lastname, organization, country, firstname, province, phone, city } = soummission;
    const id = 0;
    const sql = "INSERT INTO Submissions(firstname,lastname,email,address,city,province,country,phone,organization,comment) values(?,?,?,?,?,?,?,?,?,?)";
    return new Promise((resolve, reject) => main_1.connection.query(sql, [firstname, lastname, email, address, city, province, country, phone, organization, comment], (err, res) => {
        console.log(err);
        if (err)
            reject(err);
        resolve(res.insertId);
    }));
};
exports.getQuotes = () => {
    return new Promise((resolve, reject) => main_1.connection.query('SELECT * FROM Quotes', (err, res) => {
        if (err)
            reject(err);
        resolve(res);
    }));
};
exports.getQuote = (id) => {
    return new Promise((resolve, reject) => main_1.connection.query('SELECT * FROM Quotes where id=?', [id], (err, res) => {
        if (err)
            reject(err);
        resolve(res);
    }));
};
exports.changeQuoteStatus = (id, completed) => {
    return new Promise((resolve, reject) => main_1.connection.query('UPDATE Quotes SET completed=? WHERE id=?', [completed ? 1 : 0, id], (err, res) => {
        if (err)
            reject(err);
        resolve(res[0]);
    }));
};
exports.getQuoteProducts = (id) => {
    const sql = "CALL GetProductsFromSubmission(?)";
    return new Promise((resolve, reject) => main_1.connection.query(sql, [id], (err, res) => {
        if (err)
            reject(err);
        resolve(res[0]);
    }));
};
exports.getQuoteData = (productId) => new Promise((resolve, reject) => main_1.connection.query('CALL GetQuoteData(?)', [productId], (err, res) => {
    if (err)
        reject(err);
    resolve(res);
}));
exports.addProductToQuote = (product, id) => {
    main_1.connection.query('INSERT INTO SubmissionsProducts(idProduct,idSubmission,quantity) values(?,?,?);', [product.product.id, id, product.quantity], (err, res) => {
        if (err)
            throw err;
        return res;
    });
};
