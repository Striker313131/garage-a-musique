"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const main_1 = require("./main");
exports.getBrands = () => {
    return new Promise((resolve, reject) => {
        main_1.connection.query('SELECT * FROM Brands', (err, res) => {
            if (err)
                reject(err);
            resolve(res);
        });
    });
};
exports.getBrand = (id) => {
    return new Promise((resolve, reject) => {
        main_1.connection.query('SELECT * FROM Brands WHERE Brands.id=?', [id], (err, res) => {
            if (err)
                reject(err);
            resolve(res);
        });
    });
};
exports.getBrandByName = (brandName) => {
    return new Promise((resolve, reject) => {
        main_1.connection.query('SELECT * FROM Brands WHERE name=?', [brandName], (err, res) => {
            if (err)
                reject(err);
            resolve(res);
        });
    });
};
exports.addBrand = ({ name, logo }) => {
    return new Promise((resolve, reject) => main_1.connection.query('INSERT INTO Brands(name,logo) values(?,?)', [name, logo], (err, res) => {
        if (err)
            reject(err);
        resolve(res);
    }));
};
exports.deleteBrand = (id) => {
    return new Promise((resolve, reject) => main_1.connection.query('DELETE FROM Brands WHERE id=?', [id], (err, res) => {
        if (err)
            reject(err);
        resolve(res);
    }));
};
exports.updateBrand = ({ id, logo, name }) => {
    return new Promise((resolve, reject) => main_1.connection.query('UPDATE Brands SET logo=?, name=? WHERE id=?', [logo, name, id], (err, res) => {
        if (err)
            reject(err);
        resolve(res);
    }));
};
