"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const main_1 = require("./main");
const productsLimit = 25;
exports.getProduct = (id) => {
    return new Promise((resolve, reject) => {
        main_1.connection.query('SELECT Products.id, Products.brand, Brands.name as brandName, Products.model, Products.price, Products.description, Products.subCategoryId as subCategoryTitle, Products.imageLink, Products.msrp, Products.sku, SubCategories.title FROM Products LEFT JOIN Brands on Products.brand=Brands.id LEFT JOIN SubCategories on Products.subCategoryId=Subcategories.id WHERE Products.id=?', [id], (err, res) => {
            if (err)
                reject(err);
            resolve(res);
        });
    });
};
exports.getProducts = () => {
    return new Promise((resolve, reject) => {
        main_1.connection.query('SELECT Products.id, Products.brand, Brands.name as brandName, Products.model, Products.price, Products.description, Products.subCategoryId as subCategoryTitle, Products.imageLink, Products.msrp, Products.sku, SubCategories.title FROM Products LEFT JOIN Brands on Products.brand=Brands.id LEFT JOIN SubCategories on Products.subCategoryId=Subcategories.id', (err, res) => {
            if (err)
                reject(err);
            resolve(res);
        });
    });
};
exports.addProduct = async ({ model, brand, description, imageLink, price, subCategoryId, sku, msrp }) => {
    return await new Promise((resolve, reject) => {
        main_1.connection.query('INSERT INTO Products(brand,model,price,description,subCategoryId,imageLink,sku,msrp) values(?,?,?,?,?,?,?,?)', [brand, model || description, price || 0, description, subCategoryId, imageLink, sku, msrp || 0.0], (err, res) => {
            if (err)
                reject(err);
            resolve(res);
        });
    });
};
exports.updateProduct = async ({ id, model, brand, description, imageLink, price, subCategoryId, sku, msrp }) => {
    return await new Promise((resolve, reject) => {
        main_1.connection.query('UPDATE Products SET brand=?,model=?,price=?,description=?,subCategoryId=?,imageLink=?,sku=?,msrp=? WHERE id=?', [brand, model || description, price || 0, description, subCategoryId, imageLink, sku, msrp, id || 0.0], (err, res) => {
            if (err)
                reject(err);
            resolve(res);
        });
    });
};
exports.deleteProduct = (id) => {
    return new Promise((resolve, reject) => main_1.connection.query('DELETE FROM Products WHERE id=?', [id], (err, res) => {
        if (err)
            reject(new Error("Erreur lors de la suppression d'un produit"));
        resolve(res);
    }));
};
