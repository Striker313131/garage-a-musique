"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const main_1 = require("./main");
exports.login = (username) => {
    return new Promise((resolve, reject) => {
        main_1.connection.query('SELECT * FROM Users WHERE username=?;', [username], (err, res) => {
            if (err)
                reject(err);
            resolve(res);
        });
    });
};
exports.register = (username, password, email) => {
    return new Promise((resolve, reject) => {
        main_1.connection.query('INSERT INTO Users(username,hash,email,admin) values(?,?,?,0);', [username, password, email], (err, res) => {
            if (err)
                reject(err);
            resolve(res);
        });
    });
};
