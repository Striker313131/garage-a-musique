"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const main_1 = require("./main");
exports.getProductsUsed = () => {
    return new Promise((resolve, reject) => main_1.connection.query('SELECT * FROM ProductsUsed', (err, res) => {
        if (err)
            reject(err);
        resolve(res);
    }));
};
exports.getProductUsed = (id) => {
    return new Promise((resolve, reject) => main_1.connection.query('SELECT * FROM ProductsUsed WHERE id=?', [id], (err, res) => {
        if (err)
            reject(err);
        resolve(res);
    }));
};
exports.deleteProductUsed = (id) => {
    return new Promise((resolve, reject) => main_1.connection.query('DELETE FROM ProductsUsed WHERE id=?', [id], (err, res) => {
        if (err)
            reject(err);
        resolve(res);
    }));
};
exports.addProductUsed = ({ brand, model, description, imageLink, price }) => {
    return new Promise((resolve, reject) => {
        main_1.connection.query('INSERT INTO ProductsUsed(brand,model,description,price,imageLink) values(?,?,?,?,?)', [brand, model, description, price, imageLink], (err, res) => {
            if (err)
                reject(err);
            resolve(res);
        });
    });
};
exports.updateProductUsed = ({ id, brand, model, description, imageLink, price }) => {
    return new Promise((resolve, reject) => {
        main_1.connection.query('UPDATE ProductsUsed SET brand=?,model=?,description=?,price=?,imageLink=? WHERE id=?', [brand, model, description, price, imageLink, id], (err, res) => {
            if (err)
                reject(err);
            resolve(res);
        });
    });
};
