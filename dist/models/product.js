"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductsTest = [
    {
        id: 1,
        brand: 1,
        model: "Tesla Model 3",
        price: 55990,
        description: "Standard Range Plus",
        subCategoryId: 1,
        imageLink: "https://images.hgmsites.net/hug/2020-tesla-model-3_100736262_h.jpg",
        sku: "abcd-1234",
        msrp: 250
    },
    {
        id: 2,
        brand: 1,
        model: "Tesla Model S",
        price: 137990,
        description: "Powerful P100D Tesla Model S",
        subCategoryId: 1,
        imageLink: "https://images.hgmsites.net/hug/2020-tesla-model-s_100736252_h.jpg",
        sku: "abcd-1235",
        msrp: 350
    },
    {
        id: 3,
        brand: 1,
        model: "Tesla Model X",
        price: 119990,
        description: "Powerful Family SUV",
        subCategoryId: 1,
        imageLink: "https://images6.alphacoders.com/942/thumb-1920-942554.jpg",
        sku: "abcd-1236",
        msrp: 350
    },
    {
        id: 4,
        brand: 1,
        model: "Tesla Model Y",
        price: 75990,
        description: "A really practical SUV",
        subCategoryId: 1,
        imageLink: "https://cimg0.ibsrv.net/ibimg/hgm/1920x1080-1/100/695/2020-tesla-model-y_100695312.jpg",
        sku: "abcd-1237",
        msrp: 350
    },
    {
        id: 5,
        brand: 1,
        model: "Tesla Roadster 2020",
        price: 257000,
        description: "The fastest commercial car ever invented.",
        subCategoryId: 1,
        imageLink: "https://wallpaperaccess.com/full/865441.jpg",
        sku: "abcd-1238",
        msrp: 350
    },
    {
        id: 6,
        brand: 1,
        model: "Tesla Cybertruck",
        price: 55900,
        description: "The best electric truck available on the market",
        subCategoryId: 1,
        imageLink: "https://cdn.motor1.com/images/mgl/mLGyo/s1/tesla-cybertruck-official-image.jpg",
        sku: "abcd-1238",
        msrp: 350
    },
    {
        id: 7,
        brand: 1,
        model: "Tesla Semi Truck",
        price: 210000,
        description: "The best electric truck available on the market",
        subCategoryId: 1,
        imageLink: "https://img.wallpapersafari.com/desktop/1920/1080/62/48/auU2BV.jpg",
        sku: "abcd-1239",
        msrp: 350
    },
    {
        id: 8,
        brand: 1,
        model: "Tesla Semi Truck",
        price: 210000,
        description: "The best electric truck available on the market",
        subCategoryId: 1,
        imageLink: "https://img.wallpapersafari.com/desktop/1920/1080/62/48/auU2BV.jpg",
        sku: "abcd-1240",
        msrp: 350
    },
    {
        id: 9,
        brand: 1,
        model: "Audi R8",
        price: 189000,
        description: "Super good gas car",
        subCategoryId: 1,
        imageLink: "https://wallpaperset.com/w/full/b/b/3/501484.jpg",
        sku: "abcd-1241",
        msrp: 350
    }
];
