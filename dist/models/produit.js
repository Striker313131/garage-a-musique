"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductsTest = [
    {
        id: 1,
        marque: 1,
        modele: "Tesla Model 3",
        prix: 55990,
        description: "Standard Range Plus",
        sousCategorieId: 1,
        imageLink: "https://images.hgmsites.net/hug/2020-tesla-model-3_100736262_h.jpg"
    },
    {
        id: 2,
        marque: 1,
        modele: "Tesla Model S",
        prix: 137990,
        description: "Powerful P100D Tesla Model S",
        sousCategorieId: 1,
        imageLink: "https://images.hgmsites.net/hug/2020-tesla-model-s_100736252_h.jpg"
    },
    {
        id: 3,
        marque: 1,
        modele: "Tesla Model X",
        prix: 119990,
        description: "Powerful Family SUV",
        sousCategorieId: 1,
        imageLink: "https://images6.alphacoders.com/942/thumb-1920-942554.jpg"
    },
    {
        id: 4,
        marque: 1,
        modele: "Tesla Model Y",
        prix: 75990,
        description: "A really practical SUV",
        sousCategorieId: 1,
        imageLink: "https://cimg0.ibsrv.net/ibimg/hgm/1920x1080-1/100/695/2020-tesla-model-y_100695312.jpg"
    },
    {
        id: 5,
        marque: 1,
        modele: "Tesla Roadster 2020",
        prix: 257000,
        description: "The fastest commercial car ever invented.",
        sousCategorieId: 1,
        imageLink: "https://wallpaperaccess.com/full/865441.jpg"
    },
    {
        id: 6,
        marque: 1,
        modele: "Tesla Cybertruck",
        prix: 55900,
        description: "The best electric truck available on the market",
        sousCategorieId: 1,
        imageLink: "https://cdn.motor1.com/images/mgl/mLGyo/s1/tesla-cybertruck-official-image.jpg"
    },
    {
        id: 7,
        marque: 1,
        modele: "Tesla Semi Truck",
        prix: 210000,
        description: "The best electric truck available on the market",
        sousCategorieId: 1,
        imageLink: "https://img.wallpapersafari.com/desktop/1920/1080/62/48/auU2BV.jpg"
    },
    {
        id: 8,
        marque: 1,
        modele: "Tesla Semi Truck",
        prix: 210000,
        description: "The best electric truck available on the market",
        sousCategorieId: 1,
        imageLink: "https://img.wallpapersafari.com/desktop/1920/1080/62/48/auU2BV.jpg"
    },
    {
        id: 9,
        marque: 1,
        modele: "Audi R8",
        prix: 189000,
        description: "Super good gas car",
        sousCategorieId: 1,
        imageLink: "https://wallpaperset.com/w/full/b/b/3/501484.jpg"
    }
];
