"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv = __importStar(require("dotenv"));
const nodemailer = __importStar(require("nodemailer"));
dotenv.config();
async function sendEmail(emailInfos) {
    nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: process.env.SERVER_EMAIL,
            pass: process.env.SERVER_PASS
        }
    })
        .sendMail({
        from: '"Dev Garage à musique 👻" <eternitysgames@gmail.com>',
        to: `${process.env.SUPPORT_EMAIL}`,
        subject: "Support - Garage à musique",
        text: "",
        html: formatText(emailInfos) // html body
    }, (err, info) => {
        if (err) {
            throw Error("Erreur lors de l'envoi");
        }
    });
}
exports.sendEmail = sendEmail;
exports.sendSubmission = (client, products) => {
    nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: process.env.SERVER_EMAIL,
            pass: process.env.SERVER_PASS
        }
    })
        .sendMail({
        from: '"Dev Garage à musique 👻" <eternitysgames@gmail.com>',
        to: `${process.env.SUPPORT_EMAIL}`,
        subject: "Support - Garage à musique",
        text: "",
        html: formatSubmission(client, products) // html body
    }, (err, info) => {
        if (err) {
            throw Error("Erreur lors de l'envoi");
        }
    });
};
const formatSubmission = (client, products) => {
    return `
  <style>
  .header{
    background-color:gray
  }
  .row{
    border:1px solid black;
    padding: 2px 10px;
  }
  </style>
  <h3>Nouvelle Soumission</h3><br/>
  <table class="tg">
    <thead>
      <tr>
        <th class="header">Model</th>
        <th class="header">Marque</th>
        <th class="header">Sous-catégorie</th>
        <th class="header">Catégorie</th>
        <th class="header">Quantité</th>
      </tr>
    </thead>
    <tbody>
      ${products.map((product) => {
        return `
            <tr>
            <td class="row">${product.product.model}</td>
            <td class="row">${product.product.brandName}</td>
            <td class="row">${product.product.subCategoryName}</td>
            <td class="row">${product.product.category}</td>
            <td class="row">${product.quantity}</td>
          </tr>
          `;
    })}
    </tbody>
  </table>
  <br/><br/>
  <span>Prénom:<b> ${client.firstname} </b></span><br/>
  <span>Nom:<b> ${client.lastname} </b></span><br/>
  <span>Email:<b> ${client.email} </b></span><br/>
  <span>Adresse:<b> ${client.address} </b></span><br/>
  <span>Ville:<b> ${client.city} </b></span><br/>
  <span>Province:<b> ${client.province} </b></span><br/>
  <span>Pays:<b> ${client.country} </b></span><br/>
  <span>Téléphone:<b> ${client.phone} </b></span><br/>
  <span>Organisation:<b> ${client.organization} </b></span><br/>
  <span>Commentaire:<b> ${client.comment} </b></span><br/>
  `;
};
function formatText(emailinfos) {
    return `
  <p>Nom: ${emailinfos.nom}</p>
  <p>Email: ${emailinfos.email}</p>
  ${emailinfos.tel ? `<p>Téléhpone: ${emailinfos.tel}` : ""}
  <p>Message: ${emailinfos.text}</p>
  `;
}
