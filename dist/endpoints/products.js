"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = require("../app");
const products = __importStar(require("../database/products"));
/* Page de Contact */
app_1.router.get('/products/', (req, res) => {
    const p = products.getProducts();
    res.status(200).json({
        products
    });
});
