"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = require("../app");
const Brands = __importStar(require("../database/brands"));
app_1.app.get('/brands/', (req, res) => {
    res.status(200).json({
        ...Brands.getBrands()
    });
});
// app.get('/brands/category', (req,res) => {
//     res.status(200).json({
//         ...Brands.
//     })
// })
